package com.movirtu.diameter.test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.DisconnectCause;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Stack;

import com.movirtu.diameter.scapv2.client.CCRClient;
import com.movirtu.diameter.scapv2.server.Server;

public class Tester {

	private Server serverNode1,serverNode2;

	URI serverNode1ConfigURI;

	public void setUpServer1() throws Exception {
		try {
			this.serverNode1 = new Server();
			boolean isCustomConfigPath = true;
			URI configURI =null;

			String customConfigPath=  "/Users/prashant/workspace/diameter/diameter-scapv2/config-server-node1.xml";

			if (isCustomConfigPath) {
				try {
					configURI = new URI("file:" + customConfigPath);
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}

			if (!isCustomConfigPath || customConfigPath == null) {
				serverNode1.init(
						// new FileInputStream(new File(this.clientConfigURI)),
						getClass().getResourceAsStream("/config-server-node1.xml"),
						"SERVER");
			} else {
				serverNode1.init(new FileInputStream(new File(
						configURI)), "SERVER");
			}

			this.serverNode1.start();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void setUpServer2() throws Exception {
		try {
			this.serverNode2 = new Server();

			this.serverNode2.init(
					getClass().getResourceAsStream("/config-server-node2.xml"),"SERVER2");
			this.serverNode2.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		if (this.serverNode1 != null) {
			try {
				this.serverNode1.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode1 = null;
		}

		if (this.serverNode2 != null) {
			try {
				this.serverNode2.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.serverNode2 = null;
		}
	}

}
