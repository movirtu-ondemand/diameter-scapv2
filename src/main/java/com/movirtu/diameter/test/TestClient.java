package com.movirtu.diameter.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;

import com.movirtu.diameter.scapv2.client.CCRController;

public class TestClient {
	public static void menu() {
		System.out.println("Enter 'c'= to connect");
		System.out.println("Enter 's'= to send CCR");
		System.out.println("Enter 'q'= to exit");
		System.out.println("Enter 'x'= to close connection");
		System.out.println("Enter 'p'= to check peer connection");
		System.out.println("Enter 'h'= to show help");
	}

	public static void main(String[] args) {
		try {

			BufferedReader inFromUser = new BufferedReader(
					new InputStreamReader(System.in));

			String aMSISDN, bMSISDN, aIMSI, bIMSI, vLR, configPath;
			boolean useCustomConfig, isSC, useDefaultSC;

			if (args.length == 9) {

				if (args[0].compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				configPath = args[1];

				aMSISDN = args[2];
				bMSISDN = args[3];
				aIMSI = args[4];
				bIMSI = args[5];
				vLR = args[6];
				if (args[7].compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}

				if (args[8].compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}

				// System.out
				// .println("Usage:<aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=0/1><useDefaultshortcode=0/1>");
				// CCRController.INSTANCE.tearDown();

			} else {
				System.out
						.println("Usage:<use custom config=t/f> <path> <aMSISDN> <bMSISDN> <aIMSI> <bIMSI> <vlr> <shortcode=t/f><useDefaultshortcode=t/f>");

				System.out.println("Enter use custom config=t/f");
				String temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useCustomConfig = true;
				} else {
					useCustomConfig = false;
				}
				if (useCustomConfig) {
					System.out.println("Enter config path");
					configPath = inFromUser.readLine();
				}else{
					configPath="NULL";
				}

				System.out.println("Enter aMSISDN");
				aMSISDN = inFromUser.readLine();
				System.out.println("Enter bMSISDN");
				bMSISDN = inFromUser.readLine();
				System.out.println("Enter aIMSI");
				aIMSI = inFromUser.readLine();
				System.out.println("Enter bIMSI");
				bIMSI = inFromUser.readLine();
				System.out.println("Enter vlr");
				vLR = inFromUser.readLine();
				System.out.println("Enter if msisdn is a shortcode =t/f");
				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					isSC = true;
				} else {
					isSC = false;
				}
				System.out.println("Enter if use default shortcode =t/f");

				temp = inFromUser.readLine();
				if (temp.compareTo("t") == 0) {
					useDefaultSC = true;
				} else {
					useDefaultSC = false;
				}

				System.out.println("Entered values: "+useCustomConfig+" "+configPath+" " + aMSISDN + " " + bMSISDN
						+ " " + aIMSI + " " + bIMSI + " " + vLR + " " + isSC
						+ " " + useDefaultSC);

			}

			//set config.xml path
			CCRController.INSTANCE.isCustomConfigPath = useCustomConfig;
			CCRController.INSTANCE.customConfigPath = configPath;
			
			
			// client stack initialize and start connection
			CCRController.INSTANCE.setUp();
			// check if connected
			CCRController.INSTANCE.chkPeerConnection();

			// CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
			// bIMSI, vLR, isSC, useDefaultSC);

			for (;;) {
				menu();
				String input = inFromUser.readLine();

				if (input.compareTo("s") == 0) {
					System.out.println("sending ccr ...");
					CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
							bIMSI, vLR, isSC, useDefaultSC,"200",4);
				} else if (input.compareTo("c") == 0) {
					CCRController.INSTANCE.start();

				} else if (input.compareTo("q") == 0) {
					CCRController.INSTANCE.tearDown();
					break;

				} else if (input.compareTo("x") == 0) {
					CCRController.INSTANCE.tearDown();

				} else if (input.compareTo("p") == 0) {
					CCRController.INSTANCE.chkPeerConnection();

				} else if (input.compareTo("h") == 0) {
					menu();

				} else {
					// do nothing
				}
			}

			// new CountDownLatch(1).await();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done.....");

	}
}
