package com.movirtu.diameter.scapv2.client;

public class DiameterEvent {

	private int eventCode;

	public DiameterEvent(int eventCode) {
		this.eventCode = eventCode;
	}

	public int getEventCode() {
		return eventCode;
	}

}
