package com.movirtu.diameter.scapv2.client;

import java.util.List;

import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.OverloadException;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.RouteException;
import org.jdiameter.api.app.AppAnswerEvent;
import org.jdiameter.api.app.AppRequestEvent;
import org.jdiameter.api.app.AppSession;
import org.jdiameter.api.auth.events.ReAuthRequest;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.events.JCreditControlAnswer;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.movirtu.diameter.scapv2.common.Utils;
import com.movirtu.diameter.scapv2.common.response.CreditControlAnswerEvent;

public class CCRClient extends BaseClient {

	private static final Logger logger = LoggerFactory.getLogger(CCRClient.class);

	protected boolean sentINITIAL;
	protected boolean sentINTERIM;
	protected boolean sentTERMINATE;
	protected boolean sentEVENT;
	protected boolean receiveINITIAL;
	protected boolean receiveINTERIM;
	protected boolean receiveTERMINATE;
	protected boolean receiveEVENT;

	public CCRClient() {

	}

	private Peer getRemotePeer() throws Exception {
		Peer remotepeer = null;
		List<Peer> peers = getStack().unwrap(PeerTable.class).getPeerTable();
		boolean foundConnected = false;
		if (peers.size() >= 1) {
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					foundConnected = true;
					remotepeer = p;// use 1st connected peer i.e. overwrite
					// redundancy mode (to-do: other redundancy
					// modes)
					break;
				}
			}

		}

		if (!foundConnected)
			throw new Exception("no connected peer ...unable to send CCR");

		return remotepeer;
	}

	public String sendInitial() throws Exception {
		String sessionID = super.createCCASession("Movirtu");
		ClientCCASession ccasession = super.fetchSession(sessionID);
		/*
		 * JCreditControlRequest ccr = new CCRBuilder(ccasession,
		 * super.getApplicationId(), super.getClientRealmName(),
		 * super.getServerRealmName(), super.getClientURI(),
		 * super.getServerURI(), "SCAP_V.2.0@ericsson.com").initialCCR();
		 * ccasession.sendCreditControlRequest(ccr); Utils.printMessage(log,
		 * super.stack.getDictionary(), ccr.getMessage(), true);
		 */
		this.sentINITIAL = true;
		return sessionID;
	}

	public void sendInterim() throws Exception {
		if (!receiveINITIAL) {
			throw new Exception();
		}
		// this.ccRequestNumber++;
		this.sentINTERIM = true;
	}

	public void sendTermination() throws Exception {
		if (!receiveINTERIM) {
			throw new Exception();
		}
		// this.ccRequestNumber++;

		this.sentTERMINATE = true;
	}

	public String sendEvent(String aMSISDN, String bMSISDN, String aIMSI,
			String bIMSI, String vLR, boolean isShortCode,
			boolean defaultShortCode, String service_provider_id,
			int service_identifier) throws Exception {

		String sessionID = super.createCCASession("Movirtu");
		ClientCCASession ccasession = super.fetchSession(sessionID);
		Peer localpeer = getStack().getMetaData().getLocalPeer();
		Peer remotepeer = getRemotePeer();

		// private ApplicationId(long vendorId, long authAppId, long acctAppId)
		// createByAuthAppId(long vendorId, long authAppId) {
		// ApplicationId createByAccAppId(long vendorId, long acchAppId) {

		ApplicationId applicationId = ApplicationId.createByAuthAppId(0, 4);
		// ApplicationId applicationId = null;
		// considering single application only for time-being
		for (ApplicationId appId : remotepeer.getCommonApplications()) {
			System.out.println("AppicationId= " + appId);
			// applicationId = appId;
			break;

		}

		JCreditControlRequest ccr = new CCRBuilder(ccasession, applicationId,
				localpeer.getRealmName(), remotepeer.getRealmName(), localpeer
				.getUri().getFQDN(), remotepeer.getUri().getFQDN(),
				"SCAP_V.2.0@ericsson.com", aMSISDN, bMSISDN, aIMSI, bIMSI, vLR,
				isShortCode, defaultShortCode, service_provider_id,
				service_identifier).eventCCR();
		System.out.println("AppicationId= " + applicationId
				+ " ClientRealmName= " + localpeer.getRealmName()
				+ " ClientURI=" + localpeer.getUri().toString() + " ServerURI="
				+ remotepeer.getUri().toString() + " ServerRealmName = "
				+ remotepeer.getRealmName());
		ccasession.sendCreditControlRequest(ccr);
		Utils.printMessage(log, super.stack.getDictionary(), ccr.getMessage(),
				true);

		this.sentEVENT = true;
		return sessionID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doCreditControlAnswer(
	 * org.jdiameter.api.cca.ClientCCASession,
	 * org.jdiameter.api.cca.events.JCreditControlRequest,
	 * org.jdiameter.api.cca.events.JCreditControlAnswer)
	 */
	public void doCreditControlAnswer(ClientCCASession session,
			JCreditControlRequest request, JCreditControlAnswer answer)
					throws InternalException, IllegalDiameterStateException,
					RouteException, OverloadException {
		try {

			System.out.println("session id = "
					+ answer.getMessage().getSessionId() + " result=>"
					+ answer.getResultCodeAvp().getInteger32());

			Utils.printMessage(log, super.stack.getDictionary(),
					answer.getMessage(), false);
			switch (answer.getRequestTypeAVPValue()) {
			case CCRBuilder.CC_REQUEST_TYPE_INITIAL:
				if (receiveINITIAL) {
					fail("Received INITIAL more than once!", null);
				}
				receiveINITIAL = true;

				break;
			case CCRBuilder.CC_REQUEST_TYPE_INTERIM:
				if (receiveINTERIM) {
					fail("Received INTERIM more than once!", null);
				}
				receiveINTERIM = true;

				break;
			case CCRBuilder.CC_REQUEST_TYPE_TERMINATE:
				if (receiveTERMINATE) {
					fail("Received TERMINATE more than once!", null);
				}
				receiveTERMINATE = true;

				break;
			case CCRBuilder.CC_REQUEST_TYPE_EVENT:
				if (receiveEVENT) {
					fail("Received EVENT more than once!", null);
				}
				receiveEVENT = true;	
				handleReceivedEvent(answer);
				break;

			default:

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void handleReceivedEvent(JCreditControlAnswer answer) {
		String sessionId=null;
		int eventCode=-1;

		try {
			sessionId = answer.getMessage().getSessionId();
		} catch (InternalException e) {
			e.printStackTrace();
		}

		try {
			eventCode = answer.getResultCodeAvp().getInteger32();
		} catch (AvpDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double chargedAmount = -1;
		if(eventCode ==2001) {
			chargedAmount = computeChargedAmount(answer);
		}
		listener.handleDiameterEvent(sessionId, new CreditControlAnswerEvent(eventCode, chargedAmount));
	}

	/**
	 * This method returns the charged amount for the diameter request sent for this session
	 * id. If no value is found then -1 is returned;
	 * @param answer
	 * @return
	 */
	private double computeChargedAmount(JCreditControlAnswer answer) {
		double chargedAmount = -1;
		try {
			AvpSet avpSet = answer.getMessage().getAvps();
			Avp costInfoAvp = avpSet.getAvp(Avp.COST_INFORMATION);
			AvpSet costInfoAvpSet = costInfoAvp.getGrouped();

			Avp unitValueAvp = costInfoAvpSet.getAvp(Avp.UNIT_VALUE);
			AvpSet unitValueAvpSet = unitValueAvp.getGrouped();

			Avp valueDigitAvp = unitValueAvpSet.getAvp(Avp.VALUE_DIGITS);
			long value = valueDigitAvp.getInteger32();

			Avp exponentAvp = unitValueAvpSet.getAvp(Avp.EXPONENT);
			int exponent = exponentAvp.getInteger32();

			chargedAmount = value*Math.pow(10,exponent);

		} catch (InternalException e) {
			logger.error("InternalException in computeChargedAmount" ,e);
		} catch (AvpDataException e) {
			logger.error("AvpDataException in computeChargedAmount" ,e);
		} catch (Exception e) {
			logger.error("Exception in computeChargedAmount" ,e);
		}
		return chargedAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doReAuthRequest(org.jdiameter
	 * .api.cca.ClientCCASession, org.jdiameter.api.auth.events.ReAuthRequest)
	 */
	public void doReAuthRequest(ClientCCASession session, ReAuthRequest request)
			throws InternalException, IllegalDiameterStateException,
			RouteException, OverloadException {
		fail("Received \"ReAuthRequest\" event, request[" + request
				+ "], on session[" + session + "]", null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jdiameter.api.cca.ClientCCASessionListener#doOtherEvent(org.jdiameter
	 * .api.app.AppSession, org.jdiameter.api.app.AppRequestEvent,
	 * org.jdiameter.api.app.AppAnswerEvent)
	 */
	public void doOtherEvent(AppSession session, AppRequestEvent request,
			AppAnswerEvent answer) throws InternalException,
			IllegalDiameterStateException, RouteException, OverloadException {
		fail("Received \"Other\" event, request[" + request + "], answer["
				+ answer + "], on session[" + session + "]", null);
		listener.handleDiameterTimeoutEvent(answer.getMessage().getSessionId());

	}

	// ------------ getters for some vars;

	public boolean isSentINITIAL() {
		return sentINITIAL;
	}

	public boolean isSentEVENT() {
		return sentEVENT;
	}

	public boolean isReceiveEVENT() {
		return receiveEVENT;
	}

	public boolean isSentINTERIM() {
		return sentINTERIM;
	}

	public boolean isSentTERMINATE() {
		return sentTERMINATE;
	}

	public boolean isReceiveINITIAL() {
		return receiveINITIAL;
	}

	public boolean isReceiveINTERIM() {
		return receiveINTERIM;
	}

	public boolean isReceiveTERMINATE() {
		return receiveTERMINATE;
	}

}
