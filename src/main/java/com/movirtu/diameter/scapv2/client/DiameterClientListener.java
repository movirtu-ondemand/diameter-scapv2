package com.movirtu.diameter.scapv2.client;

public interface DiameterClientListener {
	
	public void handleDiameterEvent(String sessionId, DiameterEvent eventCode);
	
	public void handleDiameterTimeoutEvent(String sessionId);
	
}
