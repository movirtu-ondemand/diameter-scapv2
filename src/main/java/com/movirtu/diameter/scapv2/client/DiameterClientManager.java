package com.movirtu.diameter.scapv2.client;


public class DiameterClientManager {

	private String configPath;
	private boolean useCustomConfig; 
	private boolean isSC; 
	private boolean useDefaultSC;
	private String serviceProviderId="200";
	private int serviceIdentifier=4;

	public DiameterClientManager(String configPath, boolean isSC, boolean useDefaultSC, boolean useCustomConfig, DiameterClientListener listener) {
		this.configPath = configPath;
		this.useCustomConfig = useCustomConfig;
		this.isSC = isSC;
		this.useDefaultSC = useDefaultSC;
		initializeConnection();
		CCRController.INSTANCE.getClientNode().registerListener(listener);
	}

	private void initializeConnection() {
		System.out.println("Inside initializeConnection");
		System.out.println("configPath="+configPath);
		System.out.println("isSC="+isSC);
		System.out.println("useDefaultSC="+useDefaultSC);

		try {
			//set config.xml path
			CCRController.INSTANCE.isCustomConfigPath = useCustomConfig;
			CCRController.INSTANCE.customConfigPath = configPath;

			// client stack initialize and start connection
			CCRController.INSTANCE.setUp();

			// check if connected
			CCRController.INSTANCE.chkPeerConnection();

			System.out.println("Leaving initializeConnection");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String sendCCR(String aMSISDN, String bMSISDN,String aIMSI,String bIMSI,String vLR) {
		System.out.println("sending ccr ...");
		System.out.println("aMSISDN="+aMSISDN);
		System.out.println("bMSISDN="+bMSISDN);
		System.out.println("aIMSI="+aIMSI);
		System.out.println("bIMSI="+bIMSI);
		System.out.println("vLR="+vLR);

		try {
			return CCRController.INSTANCE.sendEvent(aMSISDN, bMSISDN, aIMSI,
					bIMSI, vLR, isSC, useDefaultSC,serviceProviderId,serviceIdentifier);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
