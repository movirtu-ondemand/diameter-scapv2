package com.movirtu.diameter.scapv2.client;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.jdiameter.api.ApplicationId;
import org.jdiameter.api.Avp;
import org.jdiameter.api.AvpDataException;
import org.jdiameter.api.AvpSet;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.cca.ClientCCASession;
import org.jdiameter.api.cca.events.JCreditControlRequest;
import org.jdiameter.common.impl.app.cca.JCreditControlRequestImpl;

public class CCRBuilder {

	private AvpSet ccrAvps;
	private JCreditControlRequest ccr;

	static final int CC_REQUEST_TYPE_INITIAL = 1;
	static final int CC_REQUEST_TYPE_INTERIM = 2;
	static final int CC_REQUEST_TYPE_TERMINATE = 3;
	static final int CC_REQUEST_TYPE_EVENT = 4;

	private ClientCCASession session;
	private ApplicationId appID;
	private String serverRealm;
	private String clientRealm;
	private String clientURI;
	private String serverURI;
	private String serviceContextId;
	private String aMSISDN;
	private String bMSISDN;
	private String aIMSI;
	private String bIMSI;
	private String vLR;
	private boolean isShortCode;
	private boolean defaultShortCode;
	private String service_provider_id;
	private int service_identifier;

	CCRBuilder(ClientCCASession session, ApplicationId appID,
			String clientRealm, String serverRealm, String clientURI,
			String serverURI, String serviceContextId, String aMSISDN,
			String bMSISDN, String aIMSI, String bIMSI, String vLR,
			boolean isShortCode, boolean defaultShortCode,
			String service_provider_id, int service_identifier) {
		this.session = session;
		this.appID = appID;
		this.clientRealm = clientRealm;
		this.serverRealm = serverRealm;
		this.clientURI = clientURI;
		this.serverURI = serverURI;
		this.serviceContextId = serviceContextId;
		this.aMSISDN = aMSISDN;
		this.bMSISDN = bMSISDN;
		this.aIMSI = aIMSI;
		this.bIMSI = bIMSI;
		this.vLR = vLR;
		this.isShortCode = isShortCode;
		this.defaultShortCode = defaultShortCode;
		this.service_provider_id = service_provider_id;
		this.service_identifier = service_identifier;


	}



	private void defaultCCR(int requestType) throws InternalException {

		ccr = new JCreditControlRequestImpl(session.getSessions().get(0)
				.createRequest(JCreditControlRequest.code, appID, serverRealm));
		// Create Credit-Control-Request
		// AVPs present by default: Origin-Host, Origin-Realm, Session-Id,
		// Vendor-Specific-Application-Id, Destination-Realm

		ccrAvps = ccr.getMessage().getAvps();
		// remove VENDOR_SPECIFIC_APPLICATION_ID
		// ccrAvps.removeAvp(Avp.VENDOR_SPECIFIC_APPLICATION_ID);

		// { Origin-Host }
		ccrAvps.removeAvp(Avp.ORIGIN_HOST);// one can overwrite default AVP by
		// removing it
		ccrAvps.addAvp(Avp.ORIGIN_HOST, clientURI, true, false, true);

		// [ Destination-Host ]
		ccrAvps.addAvp(Avp.DESTINATION_HOST, serverURI, true, false, false);

		// { Auth-Application-Id }
		// ccrAvps.addAvp(Avp.AUTH_APPLICATION_ID,
		// appID.getAuthAppId(),appID.getVendorId(), true, false, true);

		// { Service-Context-Id }
		// M,V
		ccrAvps.addAvp(Avp.SERVICE_CONTEXT_ID, serviceContextId, true, false,
				false);
		// { CC-Request-Type }
		ccrAvps.addAvp(Avp.CC_REQUEST_TYPE, requestType, true, false);

		// { CC-Request-Number }

		ccrAvps.addAvp(Avp.CC_REQUEST_NUMBER, 0, true, false, true);

		// [ Requested-Action ]

		ccrAvps.addAvp(Avp.REQUESTED_ACTION, 0, true, false);

		// [ Event-Timestamp ]
		// Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar c = new GregorianCalendar(1900, 0, 1);

		c.setTimeZone(TimeZone.getTimeZone("UTC"));

		long timestamp = (System.currentTimeMillis() - c.getTimeInMillis()) / 1000;

		ccrAvps.addAvp(Avp.EVENT_TIMESTAMP, timestamp, true, false, true);

		// *[ Subscription-Id ]

		AvpSet subscriptionId = ccrAvps.addGroupedAvp(Avp.SUBSCRIPTION_ID,
				true, false);

		// Subscription-Id-Type AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_TYPE, 0, true, false);// END_USER_E164

		// Subscription-Id-Data AVP
		subscriptionId.addAvp(Avp.SUBSCRIPTION_ID_DATA, aMSISDN, true, true,
				false);

		// SERVICE_IDENTIFIER
		ccrAvps.addAvp(439, service_identifier, true, false);

		// [ Requested-Service-Unit ]

		AvpSet rsuAvp = ccrAvps.addGroupedAvp(Avp.REQUESTED_SERVICE_UNIT, true,
				false);
		rsuAvp.addAvp(Avp.CC_SERVICE_SPECIFIC_UNITS, (long) 1, true, false);

		// Service-Provider-ID
		ccrAvps.addAvp(1081, service_provider_id, 193, true, false, false);
		// ccrAvps.addAvp(1081, "200", 193, true, false, false);

		// Subscription-Id-Location
		ccrAvps.addAvp(1074, vLR, 193, true, false, false);

		// Traffic-Case
		ccrAvps.addAvp(1082, 20, 193, true, false);

		// 3GPP-MS-TimeZone
		// TimeZone tz = Calendar.getInstance().getTimeZone();
		byte[] tz = { (byte) 0x0C, (byte) 0x00 };

		ccrAvps.addAvp(Avp.TGPP_MS_TIMEZONE, tz, 10415, true, false); // tigo
		// timezone

		// Service-Parameter-Info [A-Party IMSI]
		AvpSet svp = ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true,
				false);
		// Service-Parameter-Type
		svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 207, true, false);
		// Service-Parameter-Value
		svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, aIMSI, true, true, false);

		// Service-Parameter-Info [B-Party IMSI]
		// svp = ccrAvps.addGroupedAvp(Avp.SERVICE_PARAMETER_INFO, true, false);
		// Service-Parameter-Type
		// svp.addAvp(Avp.SERVICE_PARAMETER_TYPE, 208, true, false);
		// Service-Parameter-Value
		// svp.addAvp(Avp.SERVICE_PARAMETER_VALUE, bIMSI, appID.getVendorId(),
		// true, true, false);

		// Other-Party-Id
		svp = ccrAvps.addGroupedAvp(1075, 193, true, false);
		// OtherPartyIdDataTypeAvp
		svp.addAvp(1078, 0, 193, true, false);// END_USER_E164
		// (0),INTERNATIONAL (4)
		if (!isShortCode) {

			// OtherPartyIdDataAvp
			svp.addAvp(1077, bMSISDN, 193, true, false, false);
			// OtherPartyIdNatureAvp
			svp.addAvp(1076, 1, 193, true, false);/*
			 * Normal Number: International
			 * (1), ShortCode: National (2)
			 */
		} else {

			if (defaultShortCode) {
				svp.addAvp(1077, "800800", 193, true, false, false);

			} else {
				svp.addAvp(1077, bMSISDN, 193, true, false, false);
			}

			svp.addAvp(1076, 2, 193, true, false);
		}
		
	}

	public JCreditControlRequest initialCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_INITIAL);
		return ccr;

	}

	public JCreditControlRequest interimCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_INTERIM);
		return ccr;

	}

	public JCreditControlRequest terminateCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_TERMINATE);
		return ccr;

	}

	public JCreditControlRequest eventCCR() throws Exception {

		defaultCCR(CC_REQUEST_TYPE_EVENT);
		return ccr;
	}

}
