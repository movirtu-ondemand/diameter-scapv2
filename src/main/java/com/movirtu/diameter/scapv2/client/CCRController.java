package com.movirtu.diameter.scapv2.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jdiameter.api.DisconnectCause;
import org.jdiameter.api.IllegalDiameterStateException;
import org.jdiameter.api.InternalException;
import org.jdiameter.api.Mode;
import org.jdiameter.api.Peer;
import org.jdiameter.api.PeerState;
import org.jdiameter.api.PeerTable;
import org.jdiameter.api.Stack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CCRController {
	public static final CCRController INSTANCE = new CCRController();
	private CCRClient clientNode;
	protected Logger logger = LoggerFactory.getLogger(CCRController.class);

	private URI clientConfigURI;
	public boolean isCustomConfigPath;
	public String customConfigPath;

	private CCRController() {
		clientNode = null;
		isCustomConfigPath = false;
		customConfigPath = null;
	}

	public CCRClient getClientNode() {
		return clientNode;
	}

	public void setClientNode(CCRClient clientNode) {
		this.clientNode = clientNode;
	}

	public void setUp() throws Exception {

		if (isCustomConfigPath) {
			try {
				clientConfigURI = new URI("file:" + customConfigPath);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}

		if (clientNode == null) {
			clientNode = new CCRClient();
			if (!isCustomConfigPath || customConfigPath == null) {
				clientNode.init(
						// new FileInputStream(new File(this.clientConfigURI)),
						getClass().getResourceAsStream("/config-client.xml"),
						"CLIENT");
			} else {
				clientNode.init(new FileInputStream(new File(
						this.clientConfigURI)),

				"CLIENT");
			}
			start();
		} else {
			logger.warn("Diameter client already started.. you need to tear down to restart");
		}

	}

	public void start() throws IllegalDiameterStateException, InternalException {
		if (clientNode != null) {
			this.clientNode.start(Mode.ANY_PEER, 10, TimeUnit.SECONDS);
		} else {
			logger.warn("No Diameter client running .. you need to setup one first..");
		}
	}

	public void chkPeerConnection() throws Exception {
		System.out
				.println("checking peers.... supporting one connectin time-being..");
		Stack stack = this.clientNode.getStack();
		List<Peer> peers = stack.unwrap(PeerTable.class).getPeerTable();
		if (peers.size() == 1) {
			// do nothing .. everything ok
			// System.out.println(peers.get(0).getRealmName());
		} else if (peers.size() > 1) {
			// works better with replicated, since disconnected peers are
			// also listed
			boolean foundConnected = false;
			for (Peer p : peers) {

				if (p.getState(PeerState.class).equals(PeerState.OKAY)) {
					if (foundConnected) {
						throw new Exception("Wrong number of connected peers: "
								+ peers);
					}
					foundConnected = true;
				}
			}
		} else {
			throw new Exception("Wrong number of connected peers: " + peers);
		}

	}

	public void tearDown() {

		if (this.clientNode != null) {

			try {
				this.clientNode.stop(DisconnectCause.REBOOTING);
			} catch (Exception e) {

			}
			this.clientNode = null;
		} else {

			logger.warn("No Diameter client running .. you need to setup one first..");

		}

	}

	public String sendEvent(String aMSISDN, String bMSISDN, String aIMSI,
			String bIMSI, String vLR, boolean isShortCode,
			boolean defaultShortCode, String service_provider_id,
			int service_identifer) throws Exception {
		if (clientNode != null) {
			logger.info("sending CCR ..");
			return clientNode.sendEvent(aMSISDN, bMSISDN, aIMSI, bIMSI, vLR,
					isShortCode, defaultShortCode, service_provider_id,
					service_identifer);
		} else {
			logger.warn("Diameter client not running ..");
		}
		return null;
	}
}
