package com.movirtu.diameter.scapv2.common.response;

import com.movirtu.diameter.scapv2.client.DiameterEvent;


public class CreditControlAnswerEvent extends DiameterEvent {

	private double chargedAmount;

	public CreditControlAnswerEvent(int eventCode, double chargedAmount) {
		super(eventCode);
		this.chargedAmount = chargedAmount;
	}

	/**
	 * This method returns the charged amount for the diameter request sent for this session
	 * id. If no value is found then -1 is returned;
	 * @return
	 */
	public double getChargedAmount() {
		return chargedAmount;
	}

}
